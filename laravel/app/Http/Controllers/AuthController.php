<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome (Request $post) {
    	return view('welcome', $post);
    }

    public function register () {
    	return view('register');
    }
}
