<!DOCTYPE html>
<html>
<head>
	<title>Halaman Form</title>
</head>
<body>
 <h1>Buat Account Baru!</h1>

 	<form action="/welcome" method="post">
 		@csrf 
 		
 		<h2>Sign Up Form</h2>

 		<label>
 		First name :
 		<br><br>
 		<input type="text" name="nama_depan" id="nama_depan">
 	</label>
 	<br><br>
 	<label>
 		Last name :
 		<br><br>
 		<input type="text" name="nama_akhir" id="nama_akhir">
 	</label>
 	<br><br>
 	<label>
 		Gender :
 		<br><br>
 		<input type="radio" name="gender" id="gender" value="male">Male
 	</label>
 	<br>
 	<label>
 		<input type="radio" name="gender" id="gender" value="female">
 		Female
 	</label>
 		<br>
 	<label>
 		<input type="radio" name="gender" id="gender" value="other">
 		Other
 	</label>
 	<br><br>
 	<label>
 		Nationality :
 		<br><br>
 		<select name="nationality" id="nationality">
 			<option value="Indonesia">Indonesia</option>
 			<option value="Malaysia">Malaysia</option>
 			<option value="Singapura">Singapura</option>
 			<option value="Thailand">Thailand</option>
 		</select>
 	</label>
 	<br><br>
 	<label>
 		Language Spoken :
 		<br><br>
 		<input type="checkbox" name="spoke" id="spoke">Bahasa Indonesia
 	</label>
 		<br>
 	<label>
 		<input type="checkbox" name="spoke" id="spoke">English
 	</label>
 		<br>
 	<label>
 		<input type="checkbox" name="spoke" id="spoke">Other
 	</label>
 		<br>
 	
 	<br><br>
 	<label>
 		Bio :
 		<br><br>
 		<textarea rows="10" cols="30"></textarea>
 	</label>
 	<br><br>
 	<button type="submit" name="sign_up">Sign Up</button>
 	<br><br>
 	</form>
</body>
</html>